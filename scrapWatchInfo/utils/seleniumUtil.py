import time

from bs4 import BeautifulSoup
from nordvpn_switcher import rotate_VPN


def checkLoading(dri, waitUntilThisCssCanBeSelected=None):
    soup = BeautifulSoup(dri.page_source, 'html.parser')
    while '403 Forbidden' in soup.getText():
        rotate_VPN({'opsys': 'Windows', 'command': ['nordvpn', '-c', '-g'], 'settings': ['hong kong'], 'original_ip': '223.16.213.211', 'cwd_path': 'E:\\Program Files\\NordVPN'})
        dri.refresh()
        soup = BeautifulSoup(dri.page_source, 'html.parser')

    state = dri.execute_script('return document.readyState')
    dri.execute_script('console.log(document.readyState)')
    while state != "complete":
        time.sleep(1)
        state = dri.execute_script('return document.readyState')

    jQueryExist = dri.execute_script('return window.jQuery != undefined')
    dri.execute_script('console.log(window.jQuery != undefined)')
    if jQueryExist is True:
        jQueryState = dri.execute_script('return jQuery.active == 0')
        while jQueryState is not True:
            dri.execute_script('console.log(jQuery.active == 0)')
            time.sleep(1)
            jQueryState = dri.execute_script('return jQuery.active == 0')
    if waitUntilThisCssCanBeSelected is not None:
        soup = BeautifulSoup(dri.page_source, 'html.parser')
        checkCssElement = soup.select(waitUntilThisCssCanBeSelected)
        count = 0
        while len(checkCssElement) == 0 and count < 10:
            time.sleep(1)
            soup = BeautifulSoup(dri.page_source, 'html.parser')
            checkCssElement = soup.select(waitUntilThisCssCanBeSelected)
            count = count + 1
    return

def waitUntilThisCssHaveNoHiddenClass(dri, css=None):
    soup = BeautifulSoup(dri.page_source, 'html.parser')
    checkCssElement = soup.select(css)
    count = 0
    while len(checkCssElement) != 0 and count < 10:
        time.sleep(1)
        soup = BeautifulSoup(dri.page_source, 'html.parser')
        checkCssElement = soup.select(f'#hidden{css}')
        count = count + 1
    return

def waitForSpinner(dri, waitUntilThisCssDisappeared=None):
    if waitUntilThisCssDisappeared is not None:
        soup = BeautifulSoup(dri.page_source, 'html.parser')
        checkCssElement = soup.select(waitUntilThisCssDisappeared)
        count = 0
        while len(checkCssElement) == 1 and count < 60:
            time.sleep(1)
            soup = BeautifulSoup(dri.page_source, 'html.parser')
            checkCssElement = soup.select(waitUntilThisCssDisappeared)
            count = count + 1
    return
