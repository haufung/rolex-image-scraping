from django.urls import path

from . import views

urlpatterns = [
    path('health/', views.healthCheck, name='health'),
    path('watchFamilyUrl/', views.scrapWatchFamilyUrl, name='scrapWatchFamilyUrl'),
    path('watchItemUrl/', views.scrapWatchItemUrl, name='scrapWatchItemUrl'),
    path('watchItemInfo/', views.scrapWatchItemInfo, name='scrapWatchItemInfo'),
    path('watchItemHeader/', views.scrapWatchItemHeader, name='scrapWatchItemHeader'),
    path('downloadImage/', views.downloadImage, name='downloadImage'),
    path('extractCsv/', views.extractCsv, name='extractCsv'),
]
