import os
import shutil  # to save it locally

import requests  # to get image from the web
from PIL import Image
from bs4 import BeautifulSoup
from django.db import connection
from nordvpn_switcher import rotate_VPN
from selenium import webdriver

from scrapWatchInfo.utils import webDriverUtil, seleniumUtil

cur = connection.cursor()


def scrapWatchFamilyUrl(brand):
    try:
        options, chrome_driver_path = webDriverUtil.webDriverSetup()
        driver = webdriver.Chrome(options=options, executable_path=chrome_driver_path)
        cur.execute('select brand, url from watch_brand_url where brand = %s', [brand])
        resultList = cur.fetchall()
        brand = resultList[0][0]
        url = resultList[0][1]
        driver.get(url)
        watchFamilyCss = '#content > div.family-box.row > div > div > h2 > a'
        seleniumUtil.checkLoading(driver, watchFamilyCss)
        soup = BeautifulSoup(driver.page_source, 'html.parser')
        listOfWatchFamily = soup.select(watchFamilyCss)
        for family in listOfWatchFamily:
            familyName = family['href'].split("/")[-1]
            cur.execute('insert into watch_family_url ("brand", "family", "url") VALUES (%s,%s,%s)', [brand, familyName, family['href']])

        driver.quit()
        connection.close()
    except Exception as e:
        raise e

def scrapWatchItemUrl(brand, family):
    try:
        options, chrome_driver_path = webDriverUtil.webDriverSetup()
        driver = webdriver.Chrome(options=options, executable_path=chrome_driver_path)
        if family is None:
            cur.execute('select url, family from watch_family_url where brand = %s and family not in (select distinct(family) from watch_item_url  where brand = %s)', [brand,brand])
        else:
            cur.execute('select url from watch_family_url where brand = %s and family = %s', [brand, family])
        resultList = cur.fetchall()
        for result in resultList:
            if len(result) > 1:
                family = result[1]
            driver.get(result[0])
            watchItemCss = '#content > div.watch-block-container > a'
            soup = BeautifulSoup(driver.page_source, 'html.parser')
            listOfWatch = soup.select(watchItemCss)
            for watch in listOfWatch:
                cur.execute('insert into watch_item_url ("key", "brand", "family", "url") VALUES (%s,%s,%s,%s)', [watch.select('strong')[0].getText().strip('\t\r\n'),
                                                                                                                  brand,
                                                                                                                  family,
                                                                                                                  watch['href']])
        driver.quit()
        connection.close()
    except Exception as e:
        raise e


def scrapWatchItemHeader(brand):
    try:
        options, chrome_driver_path = webDriverUtil.webDriverSetup()
        driver = webdriver.Chrome(options=options, executable_path=chrome_driver_path)
        cur.execute('select url from watch_item_url where brand = %s', [brand])
        listOfUrl = cur.fetchall()
        imageCss = "#watch-detail > div.row > div.col-md-7 > div > div:nth-child(2) > div > picture > img"
        for url in listOfUrl:
            driver.get(url[0])
            seleniumUtil.checkLoading(driver, imageCss)
            soup = BeautifulSoup(driver.page_source, 'html.parser')
            headerSession = soup.select("#watch-detail > div.row > div.col-md-7 > table > tbody > tr> th")
            for header in headerSession:
                headerString = header.getText()
                strippedHeader = headerString.strip('\t\r\n:')
                cur.execute(f"select count(*) from watch_item_header_map where session = 'header' and key = %s and brand = %s", [strippedHeader, brand])
                count = cur.fetchall()
                webHeader = "header_" + strippedHeader
                uid = webHeader + "_" + brand
                if count[0][0] == 0:
                    cur.execute('''INSERT INTO watch_item_header_map("key", "session", "timestamp", "uid", "brand", "web_header") VALUES (%s,%s,NOW(),%s,%s,%s)''', [strippedHeader, "header", uid, brand, webHeader])

            caseSession = soup.select("#watch-detail > div.row > div.col-md-7 > div > div:nth-child(1) > table:nth-child(2) > tbody > tr > th")
            for case in caseSession:
                caseString = case.getText()
                strippedCase = caseString.strip('\t\r\n:')
                cur.execute("select count(*) from watch_item_header_map where session = 'case' and key = %s and brand = %s", [strippedCase, brand])
                count = cur.fetchall()
                webHeader = "case_" + strippedCase
                uid = webHeader + "_" + brand
                if count[0][0] == 0:
                    cur.execute('''INSERT INTO watch_item_header_map("key", "session", "timestamp", "uid", "brand", "web_header") VALUES (%s,%s,NOW(),%s,%s,%s)''', [strippedCase, "case", uid, brand, webHeader])
            dialSession = soup.select("#watch-detail > div.row > div.col-md-7 > div > div:nth-child(1) > table:nth-child(4) > tbody > tr > th")
            for dial in dialSession:
                dialString = dial.getText()
                strippedDial = dialString.strip('\t\r\n:')
                cur.execute("select count(*) from watch_item_header_map where session = 'dial' and key = %s and brand = %s", [strippedDial, brand])
                count = cur.fetchall()
                webHeader = "dial_" + strippedDial
                uid = webHeader + "_" + brand
                if count[0][0] == 0:
                    cur.execute('''INSERT INTO watch_item_header_map("key", "session", "timestamp", "uid", "brand", "web_header") VALUES (%s,%s,NOW(),%s,%s,%s)''', [strippedDial, "dial", uid, brand, webHeader])
            cur.execute(
                '''update watch_item_header_map w1 set table_header = w2.table_header from watch_item_header_map w2 where w1.key = w2.key and w1.session = w2.session and w1.brand = %s and w2.brand != %s''',
                [brand, brand])
            connection.commit()
        driver.quit()
        connection.close()
    except Exception as e:
        raise e


def scrapWatchItemInfo(brand, family):
    try:
        options, chrome_driver_path = webDriverUtil.webDriverSetup()
        driver = webdriver.Chrome(options=options, executable_path=chrome_driver_path)
        if family is None:
            cur.execute('select key, url, family from watch_item_url where brand = %s and url not in (select info_url from watch_item_info)', [brand])
        else:
            cur.execute('select key, url, family from watch_item_url where brand = %s and family = %s and url not in (select info_url from watch_item_info)', [brand, family])
        listOfResult = cur.fetchall()
        imageCss = "#watch-detail > div.row > div.col-md-7 > div > div:nth-child(2) > div > picture > img"
        cur.execute('select web_header, table_header from watch_item_header_map where brand = %s', [brand])
        watchItemHeaderMap = dict(cur.fetchall())
        for result in listOfResult:
            dbColumnList = []
            dbValueList = []
            if len(result) > 2:
                family = result[2]
            driver.get(result[1])
            seleniumUtil.checkLoading(driver, imageCss)
            soup = BeautifulSoup(driver.page_source, 'html.parser')
            headerSessionKey = soup.select("#watch-detail > div.row > div.col-md-7 > table > tbody > tr> th")
            headerSessionValue = soup.select("#watch-detail > div.row > div.col-md-7 > table > tbody > tr> td")
            for index, headerKey in enumerate(headerSessionKey):
                cleanHeaderKey = headerKey.getText().strip('\t\r\n:')
                cleanHeaderValue = headerSessionValue[index].getText().strip().replace("\n", " ")
                headerTableHeader = "header_" + cleanHeaderKey
                exec(f'''{watchItemHeaderMap[headerTableHeader]} = cleanHeaderValue''')
                dbColumnList.append(watchItemHeaderMap[headerTableHeader])
                exec(f'''dbValueList.append({watchItemHeaderMap[headerTableHeader]})''')

            caseSessionKey = soup.select("#watch-detail > div.row > div.col-md-7 > div > div:nth-child(1) > table:nth-child(2) > tbody > tr > th")
            caseSessionValue = soup.select("#watch-detail > div.row > div.col-md-7 > div > div:nth-child(1) > table:nth-child(2) > tbody > tr > td")
            for index, caseKey in enumerate(caseSessionKey):
                cleanCaseKey = caseKey.getText().strip('\t\r\n:')
                cleanCaseValue = caseSessionValue[index].getText().strip().replace("\n", " ")
                caseTableHeader = "case_" + cleanCaseKey
                exec(f'''{watchItemHeaderMap[caseTableHeader]} = cleanCaseValue''')
                dbColumnList.append(watchItemHeaderMap[caseTableHeader])
                exec(f'''dbValueList.append({watchItemHeaderMap[caseTableHeader]})''')

            dialSessionKey = soup.select("#watch-detail > div.row > div.col-md-7 > div > div:nth-child(1) > table:nth-child(4) > tbody > tr > th")
            dialSessionValue = soup.select("#watch-detail > div.row > div.col-md-7 > div > div:nth-child(1) > table:nth-child(4) > tbody > tr > td")
            for index, dialKey in enumerate(dialSessionKey):
                cleanDialKey = dialKey.getText().strip('\t\r\n:')
                cleanDialValue = dialSessionValue[index].getText().strip().replace("\n", " ")
                dialTableHeader = "dial_" + cleanDialKey
                exec(f'''{watchItemHeaderMap[dialTableHeader]} = cleanDialValue''')
                dbColumnList.append(watchItemHeaderMap[dialTableHeader])
                exec(f'''dbValueList.append({watchItemHeaderMap[dialTableHeader]})''')

            imageUrlSoup = soup.select_one("#watch-detail > div.row > div.col-md-7 > div > div:nth-child(2) > div > picture > img")
            image_url_low_resolution, image_url_high_resolution = None, None
            if imageUrlSoup is not None:
                image_url_low_resolution = imageUrlSoup['src']
                image_url_high_resolution = imageUrlSoup['src'].replace("/lg", "")

            dbColumn = '"' + ('","').join(dbColumnList) + '"'
            placeHolder = ['%s,'] * len(dbColumnList)

            query = f'''INSERT INTO watch_item_info({dbColumn}, "image_url_low_resolution", "image_url_high_resolution", "table_brand", "table_family", "table_key", "info_url") VALUES ({"".join(placeHolder)[:-1]},%s,%s,%s,%s,%s,%s)'''
            cur.execute(query, dbValueList + [image_url_low_resolution, image_url_high_resolution, brand, family, result[0], driver.current_url])
        driver.quit()
        connection.close()
    except Exception as e:
        raise e


def downloadImage():
    brand = 'rolex'
    try:
        cur.execute(f'select image_url_high_resolution, table_family, table_key from watch_item_info where table_brand = %s and image_url_high_resolution is not null and image_downloaded is not true', [brand])
        listOfResult = cur.fetchall()
        for result in listOfResult:
            imageUrl = result[0]
            family = result[1]
            key = result[2]
            filename = imageUrl.split("/")[-1]
            errorFromRequest = None
            r = None
            while errorFromRequest is True or errorFromRequest is None:
                try:
                    r = requests.get(imageUrl, stream=True)
                    errorFromRequest = False
                except Exception as e:
                    errorFromRequest = True
                    rotate_VPN({'opsys': 'Windows', 'command': ['nordvpn', '-c', '-g'], 'settings': ['hong kong'], 'original_ip': '223.16.213.211', 'cwd_path': 'E:\\Program Files\\NordVPN'})

            if r.status_code == 200:
                # Set decode_content value to True, otherwise the downloaded image file's size will be zero.
                r.raw.decode_content = True

                path = f'C:/Users/TANG/Desktop/watch_images/{brand}/{family}/'
                if os.path.isdir(path) is False:
                    os.mkdir(path)
                if '.jpg' in filename:
                    im1 = Image.open(r.raw)
                    filename = filename.replace(".jpg", ".png")
                    im1.save(path + filename)
                else:
                    with open(path + filename, 'wb') as f:
                        shutil.copyfileobj(r.raw, f)

                print('Image successfully Downloaded: ', filename)
                cur.execute(f'update watch_item_info set image_downloaded = true where table_key = %s',
                            [key])
            else:
                print('Image Couldn\'t be retreived')
    except Exception as e:
        raise e


def extractCsv(brand, path):
    try:
        cur.execute(f'select table_header from watch_item_header_map where brand = %s ORDER BY table_header desc', [brand])
        concatHeader = ",".join([r[0] for r in cur.fetchall()])
        productPath = path + brand + r'.csv'
        demoPath = path + brand + r'-demo.csv'
        cur.execute(f"copy (select {concatHeader} from watch_item_info where table_brand = %s) to %s with null as '' CSV HEADER;", [brand, productPath])
        cur.execute(f"copy (select {concatHeader} from watch_item_info where table_brand = %s limit 10) to %s with null as '' CSV HEADER;", [brand, demoPath])
    except Exception as e:
        raise e
